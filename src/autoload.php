<?php

spl_autoload_register(function ($identifier) {
    require_once str_replace('\\', '/', $identifier) . '.php';
});
