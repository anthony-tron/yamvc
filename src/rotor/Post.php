<?php

namespace rotor;

#[\Attribute]
class Post extends Route {
    public function __construct($route) {
        parent::__construct($route, ['POST']);
    }
}
