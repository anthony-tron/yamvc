<?php

namespace rotor;

#[\Attribute]
class Delete extends Route {
    public function __construct($route) {
        parent::__construct($route, ['DELETE']);
    }
}
