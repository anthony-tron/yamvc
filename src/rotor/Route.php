<?php

namespace rotor;

#[\Attribute]
class Route
{
    public function __construct(
        private string $_route,
        private array $_methods,
    )
    {}

    public function getRoute()
    {
        return $this->_route;
    }

    public function getMethods()
    {
        return $this->_methods;
    }
}
